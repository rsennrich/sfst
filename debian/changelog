sfst (1.4.6h-1) unstable; urgency=low

  * new upstream release
  * bump standards to 3.9.5
  * fix clang compilation error (closes: #750116)

 -- Rico Sennrich <rico.sennrich@gmx.ch>  Sun, 8 Jun 2014 17:35:54 +0000

sfst (1.4.6g-3) unstable; urgency=low

  * altivec support on powerpcspe (Closes: #701510)
  * fix shlibs-symbol-not-found build warning

 -- Rico Sennrich <rico.sennrich@gmx.ch>  Thu, 9 May 2013 10:15:04 +0100

sfst (1.4.6g-2) unstable; urgency=low

  * bump debhelper version to 9. takes care of some lintian warnings.
  * multi-arch support for shared library
  * set symlink from ChangeLog to NEWS (lintian warning)
  * add lintian override (dev-pkg-without-shlib-symlink)
    shared library has version 1, not 1-1.4

 -- Rico Sennrich <rico.sennrich@gmx.ch>  Mon, 6 May 2013 21:30:04 +0100

sfst (1.4.6g-1) experimental; urgency=low

  * new maintainer (closes: #702488)
  * new upstream release
  * update package style
      * clean-up separation of upstream/debian files
      * use source format 3.0 (quilt)
      * use autoreconf for building
      * add get-orig-source goal to rules (removes PDFs from source tarball)
  * add more headers to dev package 
    (Closes: LP:#583815)
  * fix some lintian warnings
    * fix bad whatis entries of man pages
    * add watch file
    * bump standards to 2.9.4

 -- Rico Sennrich <rico.sennrich@gmx.ch>  Tue, 16 Apr 2013 11:30:57 +0100

sfst (1.2.0-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with gcc-4.7: rename redeclared variables in src/fst.C
    (Closes: #667372)

 -- Mònica Ramírez Arceda <monica@debian.org>  Mon, 21 May 2012 13:58:07 +0200

sfst (1.2.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build depend on libreadline5-dev to libreadline-dev (Closes: #553852)
  * Set urgency to medium due to RC bug fix
  * Also fix some minor lintian warnings:
    * Add ${misc:Depends} to depends of libsfst1-1.2-0-dev
    * Bump debian/compat to 7
      * Use dh_prep instead of dh_clean -k
    * Bump standards to 2.9.2 (no further changes needed)

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Fri, 29 Apr 2011 13:46:41 +0200

sfst (1.2.0-1) unstable; urgency=low

  * Initial release Closes: #481915

 -- Francis Tyers <ftyers@prompsit.com>  Mon, 19 May 2008 11:40:31 +0100

